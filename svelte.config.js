	// import adapter from '@sveltejs/adapter-auto';

	// /** @type {import('@sveltejs/kit').Config} */
	// const config = {
	// 	kit: {
	// 		adapter: adapter()
	// 	}
	// };

	// export default config;

import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter({
			pages: 'public',
			assets: 'public',
			fallback: null
		})
	}
};

export default config;